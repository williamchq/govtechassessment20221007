Scenario 1 improvement on architecture and setup
1. use iac pipeline to scan and apply terraform 
    Reason: First, cloud engineer should not given direct access to delete or create resources. These operation should done by service account. Then, pipeline can keep track the execution log.
2. create a template repo and keep the pipeline yaml file in that repo. every app and iac pipeline should refer to the template
    Reason: when application and iac code grow in numbers, template speed up a lot as devops engineer can just update template and apply to all pipeline
3. grab a SSL certificate from ACM and put in ALB https. Then, allow https ingress in security group
    Reason: The traffic to the resources should be encrypted. 
4. harden and create AMI image.
    Reason: cloud engineer save time as he/she just need to harden once and make it a base image. Then, further spin up will use that as base image.
5. use K8s to serve application 
    Reason: K8s has ability to scale up and down. Also, make sure the pods are healthy
6. create policy to block direct commit to master branch.
    Reason: App team should not direct commit to master as any commit to master should go though build and scan first. Otherwise, App team may have chance to break the code
7. put a jumphost and allow ssh access to the web servers, it can used to troubleshoot the web servers
    Reason: Engineer should not direect access to the web servers as we cannot control what engineer can do. By using jump host, we only allow ssh and sftp access to the web servers. 
test
